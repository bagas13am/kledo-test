<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class PaymentTest extends TestCase
{
	// protected $faker;
    // use WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $this->assertTrue(true);
    // }

    public function test_insert()
    {
        $data = [
	        'payment_name' => $this->faker->words(3, true)
	    ];
	    $this->post(route('payment.create'), $data)
	        ->assertStatus(201)
	        ->assertJson(['data' => $data]);
    }

    public function test_queue_delete(){
        $this->get(route('payment.delete_queue'))
            ->assertStatus(201);
    }
}
