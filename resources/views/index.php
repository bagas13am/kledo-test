<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    </head>
    <body class="antialiased">
            <form method="post" action="/payment/create">
                Payment Name<br>
                <input type="text" name="payment_name" required>
                <button type="submit">Save</button>
            </form>

            <form method="POST" action="/payment/delete-multiple">
                <div id="app">
                  <example-component></example-component>
                </div>
                <button type="submit">Delete Multiple</button>
            </form>
            
        </div>
        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <script src="/js/app.js"></script>
    </body>
</html>
