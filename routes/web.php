<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//API
// Route::get('/payments/{offset}/{limit}', 'PaymentController@index');
// Route::post('/payments/create', 'PaymentController@create');
// Route::delete('/payments/delete', 'PaymentController@delete');
// Route::get('/execute/queue/delete', function(){
//     dispatch(new App\Jobs\DeleteQueuePayment());
//     dd('Berhasil');
// });

//Frontend
Route::get('/payment/{offset}', 'PaymentListController@index');
Route::post('/payment/create', 'PaymentListController@create');
Route::post('/payment/delete-multiple', 'PaymentListController@deleteMultiple');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/send-notif/{name}', function ($name) {
    event(new App\Events\SendGlobalNotification($name));
    return "Pusher sent";
});