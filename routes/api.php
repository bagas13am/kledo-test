<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/payments/{offset}', 'PaymentController@index')->name('payment.index');
Route::post('/payments-create', 'PaymentController@create')->name('payment.create');
Route::delete('/payments/delete', 'PaymentController@delete');
Route::get('/execute/queue/delete', function(){
    dispatch(new App\Jobs\DeleteQueuePayment());
    dd('Berhasil');
})->name('payment.delete_queue');
