<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ixudra\Curl\Facades\Curl;
use App\Models\Payment;

class PaymentListController extends Controller
{
  public function index($offset = 1) {
      return view('index');
  }

  public function create(Request $request)
  {
      //CURL DI LARAVEL 8 TIDAK BISA JALAN JIKA REQUEST CURL KE SATU PROJECT YANG SAMA
      //API diHIT dari project lain dan postman sama-sama normal

      // $data = ['payment_name' => $request->payment_name];
      // $header = ["Cache-Control: max-age=0"];
        
      // $ch = curl_init();
      // curl_setopt($ch, CURLOPT_URL, 'http://127.0.0.1:8000/api/payments/1');
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      // curl_setopt($ch, CURLOPT_ENCODING, "");
      // curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
      // curl_setopt($ch, CURLOPT_TIMEOUT, 0);
      // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      // curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
      // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      // curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      
      // $rs = curl_exec($ch);
      // if (curl_errno($ch)) {     
      //    $error_msg = curl_error($ch); 
      //    echo $error_msg; 
      //    die;
      // } 
      // curl_close($ch);
      // print_r($rs);die;
      $post = Payment::create([
          'payment_name' => $request->payment_name
      ]);
      return redirect('/payment/1');
  }

  public function deleteMultiple(Request $request)
  {
    Payment::whereIn('id', $request->payment_id)
        ->update(['is_deleted' => 1]);
    return redirect('/payment/1');
  }

}