<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Payment;

class PaymentController extends Controller
{
  public function index($offset) {
      $offset = ($offset - 1) * 10;
      $limit = 10;
      
      $data = Payment::select('id', 'payment_name', 'created_at')
        ->where(['is_deleted' => null])
        ->offset($offset)->limit($limit)->orderBy('id','desc')->get();

      $total = Payment::where(['is_deleted' => null])->count();
      $total = ceil($total/$limit);
      $meta = [];
      for ($i=1; $i <= $total; $i++) { 
        $meta[] = ['id' => $i];
      }

      return ['data' => $data, 'total' => $total, 'meta' => $meta ];
  }
  
  public function create(Request $request)
  {
      $validator = Validator::make($request->all(), [
          'payment_name'   => 'required',
      ]);
      
      if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
      }
      
      $post = Payment::create([
          'payment_name' => $request->payment_name
      ]);

      if($post) {
          return response()->json([
              'success' => true,
              'message' => 'Payment Created',
              'data'    => $post  
          ], 201);
      } 

      return response()->json([
          'success' => false,
          'message' => 'Payment Failed to Save',
      ], 409);
  }

  public function delete(Request $request)
  {
      $validator = Validator::make($request->all(), [
          'array_payment_id'   => 'required|array',
      ]);
      
      if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
      }

      Payment::whereIn('id', $request->array_payment_id)
        ->update(['is_deleted' => 1]);

      return response()->json([
          'success' => true,
          'message' => 'Payment Deleted',
      ], 200);
  }

}