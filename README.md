1. git pull origin master
2. composer update
3. php artisan migrate
4. Menambah seed baru
	php artisan db:seed
5. Mengupdate seed yang ada
	php artisan migrate:fresh --seed
6. Jalankan 
	npm run watch
7. Menjalankan Unit Test
	vendor/bin/phpunit
